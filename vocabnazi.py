import logging
from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session
import requests
import json

app_id = '357f8729'
app_key = '0b632e630188ce0c38aafb1c9830afb9'
language = 'en'
word_id = None

app = Flask(__name__)
ask = Ask(app, '/')
logging.getLogger("flask_ask").setLevel(logging.DEBUG)


@ask.launch
def start():
    welcome_msg = render_template('welcome')
    return question(welcome_msg)


@ask.intent("SearchIntent", convert={'WordMeaning': str})
def word_meaning(WordMeaning):
    
    word_id = WordMeaning
    url = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/' + language + '/' + word_id.lower()
    r = requests.get(url, headers = {'app_id': app_id, 'app_key': app_key})

    
    

@ask.intent("AnswerIntent", convert={'first': int, 'second': int, 'third': int})
def answer(first, second, third):
    winning_numbers = session.attributes['numbers']
    if [first, second, third] == winning_numbers:
        msg = render_template('win')
    else:
        msg = render_template('lose')
    return statement(msg)

if __name__ == '__main__':
    app.run(debug=True)